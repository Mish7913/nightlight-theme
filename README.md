![icon](https://gitlab.com/uploads/-/system/project/avatar/35160086/2022-04-14_09-33-27.png)

# NightLight - Gtk theme for Mate Linux

NightLight - Gtk 2.0/3.0 theme for Mate Linux.<br>

## Download
- [ ] [nightlight-theme-main.zip](https://gitlab.com/Mish7913/nightlight-theme/-/archive/main/nightlight-theme-main.zip)


## Installation
Extract zip, and move folder `nightlight-theme` in directiory ` /home/%USER_NAME%/.themes `, and move folder `nightlight-icons` in directiory ` /home/%USER_NAME%/.icons `.

Then you can choose instaled theme.


## License
GNU General Public License

